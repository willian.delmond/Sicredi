@simulacoes @all
Feature: Simulacoes

@InserirSimulacao
Scenario Outline: Inserir Simulacao
Given que executei o servico inserir simulacao para o cpf "<cpf>" no valor de <valor> em <parcelas> parcelas
Then valido status code 201
Examples:
	|		cpf			| valor	|	parcelas|
	|24098835514|1000		|4				|
	|21345546012|8374		|2				|
	
@alterarSimulacao
Scenario: Alterar Simulacao
Given que executei o servico para alterar simulacao para o cpf "11344146012" no valor de 5550 em 7 parcelas
Then valido status code 200

	
	@listarSimulacoes
	Scenario: Listar Simulacoes
	Given executei o servico de listagem de simulacoes
	Then valido status code 200
	
	@listarSimulacoesPorCPF
	Scenario Outline: Listar Simulacoes por cpf
	Given executei o servico de listagem de simulacoes para o cpf "<cpf>"
	Then valido status code 200
	And valido id da resposta
	Examples:
	|		cpf				|
	|24098835514	|
	|21345546012	|
	
#se passar id em branco ir� listar todas as simulacoes e deletar a primeira	
	@deletarSimulacao
	Scenario: Deletar Simulacoes
	Given executei o servico de deletar simulacao para o id ""
	Then valido status code 200
	
