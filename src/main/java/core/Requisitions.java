package core;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class Requisitions {

	public static RequestSpecification req;
	public static ResponseSpecification resp;
	public static final String BASE_URL = "http://localhost:8080/api/v1";
	
	public static Response getRestrictionsByCPF(String cpf) {
		return RestAssured
				.given().log().all()
				.when().get(BASE_URL+ "/restricoes/" + cpf)
				.then().extract().response();		
	}
	
	public static Response insertSimulation(String cpf, int valor, int parcelas) {
		
		String payload = "{" + 
				"    \"cpf\": \"" + cpf + "\"," + 
				"    \"nome\": \"Maria xpto\"," + 
				"    \"email\": \"maria@gmail.com\"," + 
				"    \"valor\" : \""+valor+"\"," + 
				"    \"parcelas\": \""+parcelas+"\"," + 
				"    \"seguro\": true" + 
				"}" + 
				"";
		
		return RestAssured
			.given().body(payload).contentType(ContentType.JSON)
			.when().post(BASE_URL+ "/simulacoes")
			.then().extract().response();		
	}
	
	public static Response getSimulations() {
		return RestAssured
			.given().contentType(ContentType.JSON)
			.when().get(BASE_URL+ "/simulacoes")
			.then().extract().response();		
	}
	
	public static Response getSimulationsByCPF(String cpf) {
		return RestAssured
			.given().contentType(ContentType.JSON)
			.when().get(BASE_URL+ "/simulacoes/" + cpf)
			.then().extract().response();		
	}
	
	public static Response updateSimulation(String cpf, int valor, int parcelas) {
		
		String payload = "{" + 
				"    \"cpf\": \"" + cpf + "\"," + 
				"    \"nome\": \"Maria xpto\"," + 
				"    \"email\": \"maria@gmail.com\"," + 
				"    \"valor\" : \""+valor+"\"," + 
				"    \"parcelas\": \""+parcelas+"\"," + 
				"    \"seguro\": true" + 
				"}" + 
				"";
		
		return RestAssured
			.given().body(payload).contentType(ContentType.JSON)
			.when().put(BASE_URL+ "/simulacoes/" + cpf)
			.then().extract().response();		
	}
	

	public static Response deleteSimulation(String id) {
		return RestAssured
			.given().contentType(ContentType.JSON)
			.when().delete(BASE_URL+ "/simulacoes/" + id)
			.then().extract().response();		
	}
}