package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/main/resources/features/Simulacoes.feature",
		glue = "",
		tags ="@all",
		plugin="html:target")
public class Runner {

}