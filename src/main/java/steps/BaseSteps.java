package steps;

import org.junit.Assert;

import core.Requisitions;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import io.restassured.response.Response;

public class BaseSteps {
	
	private Response response = null;
	private String cpf;
	private String id;
	
	@Entao("valido status code {int}")
	public void validoStatusCode(int expectedStatusCode) {
		Assert.assertTrue(response.statusCode() == expectedStatusCode);
	}
	
	@Entao("valido id da resposta")
	public void validoIdResposta() {
		Assert.assertTrue(response.body().asString().contains("id"));
	}
	
	@Dado("que executei o servico Obter Restricoes pelo CPF {string}")
	public void executeiServicoObterRestricosPorCPF(String cpf) {
		this.cpf = cpf;
		response = Requisitions.getRestrictionsByCPF(cpf);
	}
	
	@Entao("valido retorno 200 para cpf com restricao e 204 para sem restricao")
	public void validoStatusCode200Or204() {
		Assert.assertTrue(response.statusCode() == 200 || response.statusCode() == 204);
		
		if(response.statusCode() == 200) {
			System.out.println(response.getBody().asString());
		} else if(response.statusCode() == 204) {
			System.out.println("CPF " + cpf + "sem restricao");
		}
	}
	
	@Dado("que executei o servico inserir simulacao para o cpf {string} no valor de {int} em {int} parcelas")
	public void inserirSimulacao(String cpf, int valor, int parcelas) {
		response = Requisitions.insertSimulation(cpf, valor, parcelas);
		System.out.println(response.getStatusCode());
	}
	
	@Dado("que executei o servico para alterar simulacao para o cpf {string} no valor de {int} em {int} parcelas")
	public void alterarSimulacao(String cpf, int valor, int parcelas) {
		response = Requisitions.updateSimulation(cpf, valor, parcelas);
		System.out.println(response.getStatusCode());
	}
	
	@Dado("executei o servico de listagem de simulacoes")
	public void listarSimulacoes() {
		response = Requisitions.getSimulations();

		this.id = response.jsonPath().getString("[0].id");
	}
	
	@Dado("executei o servico de listagem de simulacoes para o cpf {string}")
	public void listarSimulacoesPorCPF(String cpf) {
		response = Requisitions.getSimulationsByCPF(cpf);
	}
	
	@Dado("executei o servico de deletar simulacao para o id {string}")
	public void deletarSimulacao(String idPassado) {
		listarSimulacoes();
		String id = this.id;
		
		if(idPassado.length() > 0) id = idPassado;
		
		response = Requisitions.deleteSimulation(id);
		System.out.println(response.getStatusCode());
	}
	
}