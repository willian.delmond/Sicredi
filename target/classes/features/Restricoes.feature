@all
Feature: Restricoes

@consultarRestricoes
Scenario Outline: Consultar restricoes por CPF
Given que executei o servico Obter Restricoes pelo CPF "<cpf>"
Then valido retorno 200 para cpf com restricao e 204 para sem restricao
Examples:
	|		cpf			|
	|97093236014|
	|60094146012|
	|84809766080|
	|62648716050|
	|26276298085|
	|01317496094|
	|55856777050|
	|19626829001|
	|24094592008|
	|58063164083|
	