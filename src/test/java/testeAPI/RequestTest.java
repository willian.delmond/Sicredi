package testeAPI;

import org.junit.Assert;
import org.junit.Test;

import core.Requisitions;
import io.restassured.response.Response;

public class RequestTest {


	public void obterRestricoesPorCPF() {
		Response res = Requisitions.getRestrictionsByCPF("26276298085");
		Assert.assertTrue(res.statusCode() == 200 || res.statusCode() == 204);
	}
	
	
	public void inserirSimulacaoComSucesso() {
		Response res = Requisitions.insertSimulation("262.762.780-85", 1000, 5);
		System.out.println(res.statusCode());
		res.prettyPrint();
		Assert.assertTrue(res.statusCode() == 201);
	}
	
	
	public void listarSimulacoes() {
		Response res = Requisitions.getSimulations();
		Assert.assertTrue(res.statusCode() == 200);
	}
	
	@Test
	public void listarSimulacoesPorCPF() {
		Response res = Requisitions.getSimulationsByCPF("999.999.999-69");
		Assert.assertTrue(res.statusCode() == 200);
		System.out.println(res.body().asString());
	}
}
